# Bitbucket Maven Artifact Uploader #

Top-level build.gradle

```
ext {
   ARTIFACT_PACKAGE = "your_lib_package"
   ARTIFACT_VERSION = "your_lib_version"
   ARTIFACT_NAME = "your_lib_name"
   ARTIFACT_PACKAGING = "aar"
}

apply from: 'https://bitbucket.org/piotrpawlowski/gradle-plugins/raw/master/bitbucket-maven-uploader.gradle'
```

Uploading
```
./gradlew uploadArchives
```

# Bitbucket Maven Artifact Downloader #

Top-level build.gradle
```
allprojects {
    repositories {
        jcenter()

        maven {
            credentials {
                username BITBUCKET_USERNAME
                password BITBUCKET_PASSWORD
            }
            url "https://api.bitbucket.org/1.0/repositories/piotrpawlowski/maven-repository/raw/releases"
        }
    }
}
```

Module-level build.gradle
```
dependencies {
    ...
    compile 'ARTIFACT_PACKAGE:ARTIFACT_NAME:ARTIFACT_VERSION'
}
```

# Git dynamic versioning #

Top-level build.gradle
```
// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:2.3.0'

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

plugins {
    id 'com.gladed.androidgitversion' version '0.3.3'
}

androidGitVersion {
    codeFormat = 'MNNPPP'
}
```

Module-level build.gradle
```
android {
    defaultConfig {
        versionName androidGitVersion.name()
        versionCode androidGitVersion.code()
        ...
    }
}
```